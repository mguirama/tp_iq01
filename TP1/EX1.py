# L'information est stocké sous forme binaire {0,1}

# Porte NOT 
# ___ ___  
#| 1 | 0 | 
#| - | - |
#| 0 | 1 |
#
# Porte AND
# ___ ___ ___ ___
#| 1 | 0 | 1 | 0 |
#| 0 | 1 | 1 | 0 | 
#| - | - | - | - |
#| 0 | 0 | 1 | 0 |
#
# Porte OR
# ___ ___ ___ ___
#| 1 | 0 | 1 | 0 |
#| 0 | 1 | 1 | 0 | 
#| - | - | - | - |
#| 1 | 1 | 1 | 0 |
#
# Porte XOR
# ___ ___ ___ ___
#| 1 | 0 | 1 | 0 |
#| 0 | 1 | 1 | 0 | 
#| - | - | - | - |
#| 1 | 1 | 0 | 0 |


# Soit l'équation suivant :
# |ψ⟩= 𝛼.|0⟩+𝛽.|1⟩
#
# |ψ⟩ représente le qubit qui peut être 
# décrit sous une combinaison linéaire de
#  |0⟩ et |1⟩
#
# |0⟩ est la représentation vectorielle du qubit avec 
# |0⟩ = |1|
#       |0|
#
# |1⟩ est la représentation vectorielle du qubit avec
# |1⟩ = |0|
#       |1|
#
# 𝛼 est un facteur tel que la probabilté que le qubit
# soit à l'état |0⟩ est de 𝛼²
#
# 𝛽 est un facteur tel que la probabilité que le qubit
# soit à l'état |1⟩ est de 𝛽²


import numpy as np
from qiskit import *

# Création d'un circuit quantique instance de QuantumCircuit
circh = QuantumCircuit(2)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circh.h(0)
# Mesure des valeurs
circh.measure_all()
# Affichage
print(circh)


# Création d'un circuit quantique instance de QuantumCircuit
circx = QuantumCircuit(2)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circx.x(0)
# Mesure des valeurs
circx.measure_all()
# Affichage
print(circx)

# Création d'un circuit quantique instance de QuantumCircuit
circswap = QuantumCircuit(2)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circswap.swap(0,1)
# Mesure des valeurs
circswap.measure_all()
# Affichage
print(circswap)

# Création d'un circuit quantique instance de QuantumCircuit
circcnot = QuantumCircuit(2)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circcnot.cx(0,1)
# Mesure des valeurs
circcnot.measure_all()
# Affichage
print(circcnot)

# Création d'un circuit quantique instance de QuantumCircuit
circcswap = QuantumCircuit(3)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circcswap.cswap(0,1,2)
# Mesure des valeurs
circcswap.measure_all()
# Affichage
print(circcswap)

# Création d'un circuit quantique instance de QuantumCircuit
circccnot = QuantumCircuit(3)
# Ajouter une porte H pou l'entrée (qubit) 0 => superposition
circccnot.toffoli(0,1,2)
# Mesure des valeurs
circccnot.measure_all()
# Affichage
print(circccnot)